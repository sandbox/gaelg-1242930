<?php
// $Id$

/*
 * Administration page
 */
function apachesolr_db_settings_page() {
  $output = drupal_get_form('apachesolr_db_settings_form');
  return $output;
}

/*
 * Administration form
 */
function apachesolr_db_settings_form() {
  $form = array();

  $form['help'] = array(
  '#type' => 'item',
  '#value' => t('Reload data config if data-config.xml has changed on the Solr server.<br/>Re-index database to reflect recent changes on it (automatically done on cron runs). This will reindex the external database in background, then replace the current database index with the new one when finished.'),
  );

  $form['reload_data_config'] = array(
    '#type' => 'submit',
    '#value' => t('Reload data config'),
    '#submit' => array('apachesolr_db_get_data_config'),
  );

  $form['reindex_database'] = array(
    '#type' => 'submit',
    '#value' => t('Re-index database'),
    '#submit' => array('apachesolr_db_update_index'),
  );

  return $form;
}