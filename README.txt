INSTRUCTIONS

* Add the mysqlconnector lib
* Create a data-config.xml file in your Solr home, and add fields in schema.xml if needed
* Modify solrconfig.xml to add
<requestHandler name="/dataimport" class="org.apache.solr.handler.dataimport.DataImportHandler">
<lst name="defaults">
  <str name="config">data-config.xml</str>
</lst>
</requestHandler>
and
<queryResponseWriter name="phps" class="org.apache.solr.request.PHPSerializedResponseWriter"/> //TODO is this really needed ?
and add "data-config.xml" in <gettableFiles>
* Restart servlet




Use hook_form_alter to improve the search form and adapt it to your content structure.



Data config basics are available at :
http://wiki.apache.org/solr/DataImportHandler#A_shorter_data-config

To be properly handled, your data-config may give this fields :
- title
- path
- teaser
- type
- name : author's name
- uid : if author is in drupal db
- body
- created (timestamp)
- changed (timestamp)


A field is declared at several places, possibly under different names :

1. In a SQL query
Directly ("SELECT phone") or with an alias ("SELECT phone AS 'Phone number'")
If no field tag is defined, this column name must match a field name (2) in the schema to be handled.

2. In the schema
<field name="phone_nb" type="string" indexed="false" stored="true" />
It must match the 'name' property of the field tag (4), or if no field tag, the column name returned by SQL (1).
Some fields names are already is the schema, such as title, type, name,...

3. In the 'column' property of a field tag
<field column="Phone number" name="phone_nb" />
It must match the column name returned by SQL (1).

4. In the 'name' property of a field tag
<field column="Phone number" name="phone_nb" />
It must match a field name in the schema (2) to be handled.

3. and 4. are needed for the search on this field to be available. Field label will be the 'column' property.
NB: the usual Drupal fields (title, type,...) do not need a field tag in data config to be handled, but they need one to enable search on it.

See data-config.example.xml for more info.