<?php
// $Id$
?>
<?php print $teaser?>
<?php if (!empty($body)) {?>
<fieldset class="collapsible collapsed">
  <legend>
    <?php print t('More info')?>
  </legend>
  <div class="fieldset-wrapper">
    <div>
      <?php print $body ?>
    </div>
  </div>
</fieldset>
<?php }; ?>